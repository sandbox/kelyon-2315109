<?php
/**
 * @file
 * localization_client_log_entity.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function localization_client_log_entity_eck_bundle_info() {
  $items = array(
  'localization_client_log_localization_client_log' => array(
  'machine_name' => 'localization_client_log_localization_client_log',
  'entity_type' => 'localization_client_log',
  'name' => 'localization_client_log',
  'label' => 'Localization Client Log',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function localization_client_log_entity_eck_entity_type_info() {
$items = array(
       'localization_client_log' => array(
  'name' => 'localization_client_log',
  'label' => 'Localization Client Log',
  'properties' => array(
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'k_translate_tracker_lid' => array(
      'label' => 'K Translate Tracker Lid',
      'type' => 'integer',
      'behavior' => 'k_translate_tracker_lid',
    ),
    'k_translate_tracker_old_string' => array(
      'label' => 'K translate tracker old string',
      'type' => 'text',
      'behavior' => 'k_translate_tracker_old_string',
    ),
    'k_translate_tracker_new_string' => array(
      'label' => 'K translate tracker new string',
      'type' => 'text',
      'behavior' => 'k_translate_tracker_new_string',
    ),
  ),
),
  );
  return $items;
}
