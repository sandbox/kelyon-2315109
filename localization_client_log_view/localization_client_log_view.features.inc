<?php
/**
 * @file
 * localization_client_log_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function localization_client_log_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
