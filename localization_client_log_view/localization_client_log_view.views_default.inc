<?php
/**
 * @file
 * localization_client_log_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function localization_client_log_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'k_translate_tracker_log_view';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_localization_client_log';
  $view->human_name = 'K Translate Tracker Log View';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'K Translate Tracker Log View';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Relationship: Localization Client Log: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Relationship: Broken/missing handler */
  $handler->display->display_options['relationships']['eck_localization_client_log_k_translate_tracker_uid_last_change']['id'] = 'eck_localization_client_log_k_translate_tracker_uid_last_change';
  $handler->display->display_options['relationships']['eck_localization_client_log_k_translate_tracker_uid_last_change']['table'] = 'users';
  $handler->display->display_options['relationships']['eck_localization_client_log_k_translate_tracker_uid_last_change']['field'] = 'eck_localization_client_log_k_translate_tracker_uid_last_change';
  $handler->display->display_options['relationships']['eck_localization_client_log_k_translate_tracker_uid_last_change']['relationship'] = 'uid';
  $handler->display->display_options['relationships']['eck_localization_client_log_k_translate_tracker_uid_last_change']['label'] = 'User last change';
  /* Field: Localization Client Log: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = 'Entity id';
  /* Field: Localization Client Log: Author */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = 'Translator uid';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Translator Username';
  /* Field: Localization Client Log: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Localization Client Log: K Translate Tracker Lid */
  $handler->display->display_options['fields']['k_translate_tracker_lid']['id'] = 'k_translate_tracker_lid';
  $handler->display->display_options['fields']['k_translate_tracker_lid']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['k_translate_tracker_lid']['field'] = 'k_translate_tracker_lid';
  $handler->display->display_options['fields']['k_translate_tracker_lid']['label'] = 'String Internal Identifier';
  $handler->display->display_options['fields']['k_translate_tracker_lid']['separator'] = '';
  /* Field: Localization Client Log: K translate tracker old string */
  $handler->display->display_options['fields']['k_translate_tracker_old_string']['id'] = 'k_translate_tracker_old_string';
  $handler->display->display_options['fields']['k_translate_tracker_old_string']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['k_translate_tracker_old_string']['field'] = 'k_translate_tracker_old_string';
  /* Field: Localization Client Log: K translate tracker new string */
  $handler->display->display_options['fields']['k_translate_tracker_new_string']['id'] = 'k_translate_tracker_new_string';
  $handler->display->display_options['fields']['k_translate_tracker_new_string']['table'] = 'eck_localization_client_log';
  $handler->display->display_options['fields']['k_translate_tracker_new_string']['field'] = 'k_translate_tracker_new_string';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'k-translate-tracker-log-view';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'K Translate Tracker';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'devel';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['k_translate_tracker_log_view'] = array(
    t('Master'),
    t('K Translate Tracker Log View'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('User'),
    t('User last change'),
    t('Entity id'),
    t('.'),
    t(','),
    t('Translator uid'),
    t('Translator Username'),
    t('Created'),
    t('String Internal Identifier'),
    t('K translate tracker old string'),
    t('K translate tracker new string'),
    t('Page'),
  );
  $export['k_translate_tracker_log_view'] = $view;

  return $export;
}
