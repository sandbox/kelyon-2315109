function bodyPadder(){
 if(jQuery('#l10n-client').hasClass('l10n-client-maximized')){//opened
  jQuery('#page').css({paddingBottom:jQuery('#l10n-client').height()});
 } else { //closed
  jQuery('#page').css('padding-bottom','0');
 }
}
jQuery(document).ready(function(){
 bodyPadder();
 jQuery("#l10n-client .toggle").click(function(){
   bodyPadder();
 });
});
(function ($) {
	Drupal.behaviors.localization_client_tracker = {
		attach: function(context, settings) {	
		  /**
		   * SUCCESS FUNCTION OF THE AJAX CALLBACK
		   */
		  function update_strings(response){
  		  if(response.status == 1){
  		    $('.string-list li').addClass("k-translate-tracker-processed");
    		  $('span.k-translate-tracker.throbber').hide();
      		$('.hide-show-tracker').attr('rel','k-translate-tracker-processed');      		  
    		  if(response.tracked_strings != 0) {
      		  for(var i = 0; i < response.tracked_strings.length; i++){
        		  $('.string-list li[rel="'+response.tracked_strings[i]+'"').removeClass('k-translate-tracker-processed');
        		  $('.string-list li[rel="'+response.tracked_strings[i]+'"').addClass('k-translate-tracker-tracked');
      		  }
    		  }
  		  }
		  }	
		  /**
		   * CLICK EVENT ON K TRANSLATE TRACKER TOGGLE
		   */
		  $('.hide-show-tracker').click(function(){
  		  if($(this).attr('rel') != 'loading'){
    		  if($(this).attr('rel') == 'k-translate-tracker-processed'){
  		      // only untracked strings are visible, show tracked strings and hide untracked strings
      		  $('.string-list li.k-translate-tracker-tracked').show();
      		  $('.string-list li.k-translate-tracker-processed').hide();
      		  $(this).attr('rel','k-translate-tracker-tracked');
      		  $(this).text(Drupal.t('Hide translated by client'));
    		  } else {
  		      // only tracked strings are visible, show untracked strings and hide tracked strings
      		  $('.string-list li.k-translate-tracker-processed').show();
      		  $('.string-list li.k-translate-tracker-tracked').hide();
      		  $(this).attr('rel','k-translate-tracker-processed');      
      		  $(this).text(Drupal.t('Show translated by client'));		  
    		  }
  		  }
		  });
		  $('.k-translate-tracker.throbber').fadeIn();
      $.ajax({
        type : 'POST',
        url : Drupal.settings.basePath + 'localization_client_tracker-get-logged-strings-ajax', // Which url should be handle the ajax request.
        success : update_strings, // The js function that will be called upon success request
        dataType : 'json', //define the type of data that is going to get back from the server
      });
      $('#l10n-client-form').submit(function(){
        $('.string-list li.active').removeClass("k-translate-tracker-processed");
        $('.string-list li.active').addClass("k-translate-tracker-tracked");
      });
    }
  }	
})(jQuery);